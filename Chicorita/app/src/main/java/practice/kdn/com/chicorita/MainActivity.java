package practice.kdn.com.chicorita;

import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button btn01;
    EditText editText;

    int picResIndex = 0;
    int currentIndex = 0;

    enum RandPic {
        PIC1, PIC2, PIC3,
        PIC4, PIC5, PIC6,
        PIC7, PIC8, PIC9
    }

    int picRes[] = {
            R.id.imgView01, R.id.imgView02, R.id.imgView03,
            R.id.imgView04, R.id.imgView05, R.id.imgView06,
            R.id.imgView07, R.id.imgView08, R.id.imgView09,
    };

    int picLoc[] = {
            R.drawable.pic1, R.drawable.pic2, R.drawable.pic3,
            R.drawable.pic4, R.drawable.pic5, R.drawable.pic6,
            R.drawable.pic7, R.drawable.pic8, R.drawable.pic8
    };

    public int pickPic(RandPic rand){
        int nan = (int)(Math.random()*9);

        RandPic temp = null;

        switch(nan){
            case 0 : temp = RandPic.PIC1; break;
            case 1 : temp = RandPic.PIC2; break;
            case 2 : temp = RandPic.PIC3; break;
            case 3 : temp = RandPic.PIC4; break;
            case 4 : temp = RandPic.PIC5; break;
            case 5 : temp = RandPic.PIC6; break;
            case 6 : temp = RandPic.PIC7; break;
            case 7 : temp = RandPic.PIC8; break;
            case 8 : temp = RandPic.PIC9; break;
        }
        Log.i("nan " , "nan = " + Integer.toString(nan));

        return nan;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ImageView imgView[]= new ImageView[9];
        editText = (EditText)findViewById(R.id.editText);

        for(int i = 0; i < imgView.length; i++){
            imgView[i] = (ImageView) findViewById(picRes[i]);
        }

        String temp ;
        btn01 = (Button)findViewById(R.id.btn01);
        btn01.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                imgView[currentIndex].setImageResource(picLoc[pickPic(RandPic.PIC1)]);
                if(currentIndex < 8) {
                    currentIndex++;
                }
                else {currentIndex = 0;}

                Toast.makeText(MainActivity.this, editText.getText(), Toast.LENGTH_SHORT).show();
                editText.setText("코코치코");
            }
        });

    }
}
