package project10_2.cookandroid.com.project10_2;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    private Button btnResult;

    private ImageView imgView[] = new ImageView[9];
    final int voteCnt[] = new int[9];

    private int imageId[] = {
            R.id.iv1, R.id.iv2, R.id.iv3,
            R.id.iv4, R.id.iv5, R.id.iv6,
            R.id.iv7, R.id.iv8, R.id.iv9,
    };

    private String imgName[] = {
        "독서하는 소녀" , "꽃장식 모자 소녀", "부채를 든 소녀",
            "이레느깡 단 베르양", "잠자는 소녀", "테라스의 두 자매",
            "피아노 레슨", "피아노 앞의 소녀들", "해변에서"
    };

    View.OnClickListener listener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            if(v.getId()== R.id.btnResult) {
                Intent intent = new Intent(MainActivity.this, ResultMain.class);
                intent.putExtra("imgName", imgName);
                intent.putExtra("voteCnt", voteCnt);
                startActivity(intent);
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnResult = (Button)findViewById(R.id.btnResult);
        btnResult.setOnClickListener(listener);

        for(int i =0; i< voteCnt.length; i++){
            voteCnt[i] = 0;
        }

        for(int i = 0; i < imageId.length; i++){
            final int index = i;
            imgView[i] = (ImageView)findViewById(imageId[i]);
            imgView[i].setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    voteCnt[index]++;
                    Toast.makeText(MainActivity.this, imgName[index] + " : 총 " + voteCnt[index] + "표",
                            Toast.LENGTH_LONG).show();
                }
            });
        }
    }
}
