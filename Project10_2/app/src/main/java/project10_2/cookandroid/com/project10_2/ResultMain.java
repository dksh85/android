package project10_2.cookandroid.com.project10_2;

import android.content.Intent;
import android.media.Rating;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

public class ResultMain extends AppCompatActivity {
    Button btnReturn;

    int tvID[] = {  R.id.tv1, R.id.tv2, R.id.tv3,
                    R.id.tv4, R.id.tv5, R.id.tv6,
                    R.id.tv7, R.id.tv8, R.id.tv9
                };

    int ratingID[] = {
                        R.id.rbar1, R.id.rbar2, R.id.rbar3,
                        R.id.rbar4, R.id.rbar5, R.id.rbar6,
                        R.id.rbar7, R.id.rbar8, R.id.rbar9
                    };

    View.OnClickListener listenerr = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.btnReturn :
                    finish();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_main);


        Intent intent = getIntent();
        int voteCnt[] = intent.getIntArrayExtra("voteCnt");
        String imgName[] = intent.getStringArrayExtra("imgName");


        TextView tv[] = new TextView[imgName.length];
        RatingBar rbar[] = new RatingBar[voteCnt.length];

        for(int i = 0; i < tv.length; i++){
            tv[i] = (TextView)findViewById(tvID[i]);
            tv[i].setText(imgName[i]);
            rbar[i] = (RatingBar)findViewById(ratingID[i]);
            rbar[i].setRating(voteCnt[i]);
        }

        btnReturn = (Button)findViewById(R.id.btnReturn);
        btnReturn.setOnClickListener(listenerr);
    }
}
