package kdn.com.androidstep03activitycycle;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class SubActivity extends AppCompatActivity {

    private TextView resultView;

    public void bntClick(View v){
        Toast.makeText(SubActivity.this, "button event", Toast.LENGTH_SHORT).show();
//        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub);
        Intent intent = getIntent();

        resultView = (TextView)findViewById(R.id.result);
        resultView.setText(intent.getStringExtra("msg"));
    }

        @Override
    protected void onStart() {
        super.onStart();
        Log.e("onStart()","SubActivity");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("onResume()","SubActivity");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("onPause()","SubActivity");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("onStop()","SubActivity");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("onDestroy()","SubActivity");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e("onRestart()","SubActivity");
    }
}
