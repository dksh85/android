package kdn.com.androidstep03activitycycle;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import model.vo.KdnVO;

public class ThirdActivity extends AppCompatActivity {
    private KdnVO kdnvo;
    private TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        Intent intent = getIntent();

        kdnvo = (KdnVO) intent.getSerializableExtra("kdn");

        result = (TextView) findViewById(R.id.result3);
        result.setText("이름 : " + kdnvo.getName() + " / " +" 부서 : " + kdnvo.getDept());

        Toast.makeText(ThirdActivity.this, kdnvo.getName(), Toast.LENGTH_LONG).show();

    }
}
