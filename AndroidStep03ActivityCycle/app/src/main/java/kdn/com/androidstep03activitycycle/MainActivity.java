package kdn.com.androidstep03activitycycle;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;

import model.vo.KdnVO;

public class MainActivity extends AppCompatActivity {
    private int cnt = 0;
    private RadioButton subRadio,   thirdRadio;
    private Button      newBtn;

    View.OnClickListener bntHandler = new View.OnClickListener(){
       public void onClick(View v){

            if(subRadio.isChecked()){
                Intent intent = new Intent(MainActivity.this, SubActivity.class);
                intent.putExtra("msg", "cheer");
                startActivity(intent);
            } else if(thirdRadio.isChecked()){
                Intent intent = new Intent(MainActivity.this, ThirdActivity.class);
                intent.putExtra("kdn", new KdnVO("섭섭해", "송배전"));
//                intent.getSerializableExtra();
                startActivity(intent);
            } else{

                AlertDialog.Builder alertDig =new AlertDialog.Builder(MainActivity.this);
                if(cnt ==0) {
                    alertDig.setMessage("뭐하냐").show();
                    cnt++;
                }
                else
                    alertDig.setMessage("아 진짜 뭐하냐고").show();
            }
        }
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        subRadio = (RadioButton)findViewById(R.id.rdoSub);
        thirdRadio = (RadioButton)findViewById(R.id.rdoThird);

        newBtn = (Button)findViewById(R.id.newActivity);
        newBtn.setOnClickListener(bntHandler);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("onStart()","MainActivity");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("onResume()","MainActivity");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("onPause()","MainActivity");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("onStop()","MainActivity");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("onDestroy()","MainActivity");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e("onRestart()","MainActivity");
    }
}
