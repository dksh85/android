package model.vo;

import java.io.Serializable;

/**
 * Created by User on 2016-04-27.
 */
public class KdnVO implements Serializable{
    private String name, dept;

    public KdnVO() {
    }

    public KdnVO(String name, String dept) {
        this.name = name;
        this.dept = dept;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }
}
