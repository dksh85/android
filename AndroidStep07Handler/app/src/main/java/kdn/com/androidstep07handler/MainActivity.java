package kdn.com.androidstep07handler;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView foreTxt, backTxt;
    private Button btn;
    private int foreCnt , backCnt;


    View.OnClickListener btnHandler = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            foreCnt++;
            foreTxt.setText("BackGroundValue : " + foreCnt);
//            foreTxt.setText("BackGroundValue : " + backCnt);
        }
    };

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if(msg.what == 0) {
//                backTxt.setText("BackGroundValue : " + backCnt);
                backTxt.setText("BackGroundValue : " + msg.arg1);
            }
        }
    };

    class BackGroundThread implements Runnable {
        @Override
        public void run() {
            while(true) {
                backCnt++;
                Message msg = new Message();
                msg.arg1 = backCnt;
                msg.what = 0;
                handler.sendMessage(msg); // 0 번으로 보낼테니 0번으로 받아라
//                backTxt.setText("BackGroundValue : " + backCnt);
//  thread 가 thread를 참조하는 것을 방지함 -> Handler가 필요함 (thread당 존재하는 통신채널)

                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        foreTxt = (TextView)findViewById(R.id.foreTxt);
        backTxt = (TextView)findViewById(R.id.backTxt);
        btn = (Button)findViewById(R.id.btn);
        btn.setOnClickListener(btnHandler);

        BackGroundThread background = new BackGroundThread(); //Thread 의 대상이 되는 객체
        Thread thread = new Thread(background);
        thread.setDaemon(true);
        thread.start();
    }
}
