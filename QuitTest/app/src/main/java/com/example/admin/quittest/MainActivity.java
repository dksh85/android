package com.example.admin.quittest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
   Button quitbutton;

    View.OnClickListener listener = new View.OnClickListener(){
        public void onClick(View v){
            if(v.getId() == R.id.quitbutton) {
                MainActivity.this.finish();
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        quitbutton = (Button)findViewById(R.id.quitbutton);

        quitbutton.setOnClickListener(listener);
    }
}
