package model.Adapter;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import kdn.com.listviewdialog.R;
import model.ArithmeticVO;

/**
 * Created by User on 2016-05-13.
 */
public class ArithmeticAdapter extends BaseAdapter{
    private Context context;
    private int layoutRes;
    private ArrayList<ArithmeticVO> aList;
    private LayoutInflater inflater;

    public ArithmeticAdapter(Context context, int layoutRes, ArrayList<ArithmeticVO> aList) {
        this.context = context;
        this.layoutRes = layoutRes;
        this.aList = aList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return aList.size();
    }

    @Override
    public Object getItem(int position) {
        return aList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = inflater.inflate(this.layoutRes, parent, false);
        }

        ImageView imgView = (ImageView)convertView.findViewById(R.id.imgView);
        TextView textView = (TextView)convertView.findViewById(R.id.txtView);

        imgView.setImageResource(aList.get(position).getImgRes());
        textView.setText(aList.get(position).getOperation());



        return convertView;
    }
}
