package kdn.com.listviewdialog;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import model.Adapter.ArithmeticAdapter;
import model.ArithmeticVO;

public class MainActivity extends AppCompatActivity {
    ArrayList<ArithmeticVO> aList;

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        aList = new ArrayList<ArithmeticVO>();

        aList.add(new ArithmeticVO("sum", R.drawable.sum));
        aList.add(new ArithmeticVO("minus", R.drawable.sum));
        aList.add(new ArithmeticVO("mult", R.drawable.sum));
        aList.add(new ArithmeticVO("div", R.drawable.sum));

        ArithmeticAdapter listAdapter = new ArithmeticAdapter(MainActivity.this, R.layout.list_item, aList);

        View forListView = View.inflate(MainActivity.this, R.layout.list_layout, null);
        listView = (ListView)forListView.findViewById(R.id.listView);
        listView.setAdapter(listAdapter);

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("App");
        builder.setView(forListView);
        builder.setNeutralButton("ok", null);
        builder.show();
    }
}
