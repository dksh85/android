package kdn.com.androidstep11fragement;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //만일 View 의 참조값이 필요하다면?
        TextView textView = (TextView)findViewById(R.id.textView);
        //만일 Fragment 의 참조값이 필요하다며?
        //FragmentManager 객체의 참조값을 얻어온다.
        FragmentManager fManager = getFragmentManager();
        //findFragmentById() 메소드를 이용해서 MyFragment 의 참조값을 얻어온다.
        MyFragment f = (MyFragment)
                fManager.findFragmentById(R.id.myFragment);
    }

    public static class MyFragment extends Fragment {
        //제어할 View 객체를 만들어서 리턴해 줘야한다.
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            //인자로 전달된 레이아웃 전개자 객체를 이용해서 View 객체를 만든다.
            View view = inflater.inflate(R.layout.fragment_my, container);
            //만든 View 객체를 리턴해준다.
            return view;
        }
    }
}
