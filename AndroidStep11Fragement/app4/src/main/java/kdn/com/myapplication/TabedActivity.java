package kdn.com.myapplication;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Locale;


public class TabedActivity extends Activity {


    SectionsPagerAdapter mSectionsPagerAdapter;

    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_tabed);

        //ViewPager 에 Fragment 를 공급할 아답타 객체생성하기
        mSectionsPagerAdapter =
                new SectionsPagerAdapter(getFragmentManager());

        //ViewPager 객체의 참조값 얻어와서 맴버필드에 저장하기
        mViewPager = (ViewPager) findViewById(R.id.pager);

        //아답타를 ViewPager 에 연결하기
        mViewPager.setAdapter(mSectionsPagerAdapter);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tabed, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /*
        ViewPager 객체에 Fragment 를 공급하기위한 클래스 정의하기

        - FragmentPagerAdapter 추상클래스를 상속받아서 만든다.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        //생성자
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        //인자로 전달되는 인덱스에 해당하는 Fragment 객체를 만들어서
        //리턴해주는 메소드
        @Override
        public Fragment getItem(int position) {
            //position 에 해당하는 Fragment 객체의 참조값을 얻어내서 리턴
            return PlaceholderFragment.newInstance(position + 1);
        }
        //전체 Fragment 갯수를 리턴하는 메소드
        @Override
        public int getCount() {

            return 3;
        }
        //페이지의 제목을 리턴해주는 메소드
        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_section1).toUpperCase(l);
                case 1:
                    return getString(R.string.title_section2).toUpperCase(l);
                case 2:
                    return getString(R.string.title_section3).toUpperCase(l);
            }
            return null;
        }
    }

    //프레그먼트를 만들 클래스
    public static class PlaceholderFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";

        //Fragment 객체를 리턴하는 Factory 메소드
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_tabed, container, false);
            TextView textView = (TextView)
                            rootView.findViewById(R.id.section_label);
            textView.setText("페이지 입니다.");
            return rootView;
        }


    }

}
