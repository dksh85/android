package kdn.com.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


public class MainActivity extends Activity
                    implements InsertFragment.Listener{
    MyListFragment myListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //MyListFragment 의 참조값을 얻어와서 맴버필드에 저장하기
        myListFragment = (MyListFragment)
           getFragmentManager().findFragmentById(R.id.myListFragment);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    //InsertFragment 가 호출하는 메소드
    @Override
    public void insertFragmentListener(String msg) {
        //인자로 전달받은 문자열을 MyListViewFragment 객체에
        //전달한다.
        myListFragment.addMsg(msg);
    }
}
