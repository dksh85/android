package kdn.com.myapplication;

import android.app.ListFragment;
import android.os.Bundle;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

/*
    ListView 를 포함하고 있는 ListFragment 를 상속받아서 만든다.
 */
public class MyListFragment extends ListFragment{
    //필요한 맴버필드 정의하기
    ArrayList<String> msgList; //모델
    ArrayAdapter<String> adapter; //아답타 객체

    //Activity 의 onCreate() 메소드가 호출된 직후 호출되는 메소드
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //프레그먼트에서 필요한 초기화 작업들을 해준다.

        msgList = new ArrayList<String>();
        msgList.add("하나");
        msgList.add("두울");

        //아답타 객체 생성하기
        adapter = new ArrayAdapter<String>(
                getActivity(),
                android.R.layout.simple_list_item_1,
                msgList
        );

        //getListView().setAdapter(adapter);

        //ListView 에 아답타 연결하기
        setListAdapter(adapter);
    }
    //모델에 데이터를 추가하고 UI 를 업데이트하는 메소드 정의하기
    public void addMsg(String msg){
        msgList.add(msg);
        adapter.notifyDataSetChanged();
    }
}
