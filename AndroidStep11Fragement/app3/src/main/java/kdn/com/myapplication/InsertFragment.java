package kdn.com.myapplication;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


public class InsertFragment extends Fragment
                        implements View.OnClickListener{

    EditText inputMsg;

    //View 를 만들어서 리턴해주는 메소드
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_insert, container, false);
        //버튼의 참조값 얻어와서
        Button addBtn = (Button)view.findViewById(R.id.addBtn);
        //리스너를 등록한다.
        addBtn.setOnClickListener(this);
        inputMsg = (EditText)view.findViewById(R.id.inputMsg);
        return view;
    }
    //추가 버튼을 눌렀을때 호출되는 메소드
    @Override
    public void onClick(View v) {
        //입력한 문자열을 읽어온다.
        String msg = inputMsg.getText().toString();
        Listener activity = (Listener)getActivity();
        activity.insertFragmentListener(msg);
    }
    //액티비티가 구현할 리스너 인터페이스
    public interface Listener{
        public void insertFragmentListener(String msg);
    }
}
