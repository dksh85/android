package kdn.com.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


public class MainActivity extends Activity
                        implements InputFragment.Listener{
    //필요한 맴버필드 정의하기
    DisplayFragment dFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        //FragmentManager 객체를 이용해서 DisplayFragment 객체의 참조값을 얻어와서
        //맴버필드에 저장한다.
        dFragment = (DisplayFragment)
                getFragmentManager().findFragmentById(R.id.displayFragment);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //InputFragment 에서 호출하는 메소드
    @Override
    public void inputFragmentListener(String msg) {
        /*
            InputFragment 객체가 전달한 데이터를
            DisplayFragment 객체에 appendMsg() 메소드를 호출하면서 전달해준다.
         */
        dFragment.appendMsg(msg);
    }
    //액티비티 이동 버튼을 눌렀을때 호출되는 메소드
    public void move(View v){
        Intent intent=new Intent(this, SubActivity.class);
        startActivity(intent);
    }
}











