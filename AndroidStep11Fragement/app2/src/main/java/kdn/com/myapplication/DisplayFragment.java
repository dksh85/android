package kdn.com.myapplication;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

/**
 * Created by lee on 2014-11-28.
 */
public class DisplayFragment extends Fragment{
    EditText console;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_display, container);
        //EditText 객체의 참조값을 얻어와서 맴버필드에 저장한다.
        console = (EditText)view.findViewById(R.id.console);
        return view;
    }
    //콘솔에 인자로 전달된 메세지를 누적시키는 메소드
    public void appendMsg(String msg){
        console.append(msg+"\n");
    }
}
