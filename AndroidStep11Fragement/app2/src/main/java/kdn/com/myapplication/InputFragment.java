package kdn.com.myapplication;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by lee on 2014-11-28.
 */
public class InputFragment extends Fragment
                        implements View.OnClickListener{
    EditText inputMsg;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //xml 문서를 전개해서 View 객체를 만든다.
        View view = inflater.inflate(R.layout.fragment_input, container);
        //버튼의 참조값을 얻어온다.
        Button sendBtn = (Button)view.findViewById(R.id.sendBtn);
        //버튼에 리스너 등록하기
        sendBtn.setOnClickListener(this);
        //EditText 의 참조값 얻어와서 맴버필드에 저장한다.
        inputMsg = (EditText)view.findViewById(R.id.inputMsg);

        //만든 View 객체의 참조값을 리턴해준다.
        return view;
    }
    //전송버튼을 눌렀을때 호출되는 메소드
    @Override
    public void onClick(View v) {
        //입력한 문자열을 읽어온다.
        String msg = inputMsg.getText().toString();
        //액티비티를 Listener Type으로 형변환 한다음
        Listener activity = (Listener)getActivity();
        //메소드를 호출하면서 데이터를 전달한다.
        activity.inputFragmentListener(msg);

    }
    //액티비티가 구현할 인터 페이스를 정의한다.
    public interface Listener{
        //추상메소드 정의하기
        public void inputFragmentListener(String msg);
    }
}










