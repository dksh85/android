package model.list.custom.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;

import kdn.com.androidstep06customadapter.DetailActivity;
import kdn.com.androidstep06customadapter.R;
import model.vo.CountryVO;

/*
    [커스텀 아답타 객체를 생성할 클래스 정의하기 ]

    - BaseAdapter 추상클래스를 상속받아서 만든다.
 */
public class CountryAdapter extends BaseAdapter{
    //필요한 맴버필드 정의하기
    private Context context;
    private int layoutRes; //cell 의 레이아웃 리소스 아이디
    private ArrayList<CountryVO> list; //모델
    private LayoutInflater inflater; //레이아웃 전개자 객체

    //생성자
    public CountryAdapter
    (Context context, int layoutRes, ArrayList<CountryVO> list){
        //생성자의 인자로 전달받은 값을 맴버필드에 저장한다.
        this.context=context;
        this.layoutRes=layoutRes;
        this.list=list;
        //레이아웃 전개자 객체를 얻어와서 맴버필드에 저장한다.
        inflater=LayoutInflater.from(context);
    }

    //모델의 전체 갯수를 리턴하는 메소드
    @Override
    public int getCount() {
        //ArrayList 의 사이즈를 리턴해준다.
        return list.size();
    }

    //인자로 전달되는 인덱스에 해당하는 모델 객체를 리턴하는 메소드
    @Override
    public Object getItem(int position) {
        //ArrayList 에서 해당 인덱스 데이터를 리턴해준다.
        return list.get(position);
    }
    //인자로 전달되는 인덱스에 해당하는 아이디가 있다면 리턴하는 메소드
    @Override
    public long getItemId(int position) {
        //특별히 아이디가 없기 때문에 그냥 position 을 리턴해준다.
        return position;
    }

    public void getButton(Button btn){

    }
    //인자로 전달되는 position 에 출력할 View 객체를 구성해서 리턴해주는 메소드
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.e("getView()", "position:"+position);
        //처음에는 convertView 에 null 이 전달된다.
        if(convertView == null){
            //레이아웃 전개자 객체를 이용해서 셀 View 를 만든다.
            convertView = inflater.inflate(layoutRes, parent, false);
            //로그에 출력해보기
            Log.e("getView()","convertView == null");
        }
        //셀에 전개된 객체의 참조값을 얻어온다.
        ImageView imageView = (ImageView)
                convertView.findViewById(R.id.imageView);
        TextView textView = (TextView)
                convertView.findViewById(R.id.textView);
        Button btn = (Button)
                convertView.findViewById(R.id.btn01);
        //position 에 해당하는 데이터를 모델에서 불러온다.
        final CountryVO dto = list.get(position);
        //셀에 데이터를 출력한다
        imageView.setImageResource(dto.getImgRes());
        textView.setText(dto.getName());
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra("detail_info",dto);
                context.startActivity(intent);
            }

        });
        //구성된 셀 뷰를 리턴해준다.
        return convertView;

    }
}


