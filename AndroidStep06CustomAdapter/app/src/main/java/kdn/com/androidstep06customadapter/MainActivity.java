package kdn.com.androidstep06customadapter;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import kdn.com.androidstep06customadapter.R;
import model.list.custom.adapter.CountryAdapter;
import model.vo.CountryVO;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    ArrayList<CountryVO> countries=new ArrayList<CountryVO>();
    CountryAdapter adapter;
    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //ListView 의 참조값 얻어와서 맴버필드에 저장하기
        listView = (ListView)findViewById(R.id.listView);
        //Model 에 Sample 데이터 넣어주기
        countries.add(new CountryVO(R.drawable.austria, "오스트리아"));
        countries.add(new CountryVO(R.drawable.belgium, "벨기에"));
        countries.add(new CountryVO(R.drawable.brazil, "브라질"));
        countries.add(new CountryVO(R.drawable.france, "프랑스"));
        countries.add(new CountryVO(R.drawable.germany, "독일"));
        countries.add(new CountryVO(R.drawable.greece, "그리스"));
        countries.add(new CountryVO(R.drawable.israel, "이스라엘"));
        countries.add(new CountryVO(R.drawable.italy, "이탈리아"));
        countries.add(new CountryVO(R.drawable.japan, "일본"));
        countries.add(new CountryVO(R.drawable.korea, "대한민국"));
        countries.add(new CountryVO(R.drawable.poland, "폴란드"));
        countries.add(new CountryVO(R.drawable.spain, "스페인"));
        countries.add(new CountryVO(R.drawable.usa, "미국"));

        adapter = new CountryAdapter(MainActivity.this, R.layout.listview_cell,countries);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final CountryVO obj = countries.get(position);
                View dialogView = (View) View.inflate(
                        MainActivity.this, R.layout.dialog, null);
                AlertDialog.Builder dlg = new AlertDialog.Builder(
                        MainActivity.this);
                ImageView ivPoster = (ImageView) dialogView.findViewById(R.id.ivPoster);
                ivPoster.setImageResource(obj.getImgRes());
                dlg.setTitle("Chose Nation");
                dlg.setView(dialogView);
                dlg.setNegativeButton("close", null);
                dlg.show();
            }
        });
    }
}
