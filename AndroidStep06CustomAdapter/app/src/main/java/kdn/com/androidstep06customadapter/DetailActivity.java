package kdn.com.androidstep06customadapter;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import model.vo.CountryVO;

public class DetailActivity extends AppCompatActivity {

    ImageView imgView;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Intent intent = getIntent();
        CountryVO countryVO = (CountryVO) intent.getSerializableExtra("detail_info");

        imgView = (ImageView) findViewById(R.id.detailImage);
        imgView.setImageResource(countryVO.getImgRes());

        textView = (TextView) findViewById(R.id.detailLocation);
        textView.setText(countryVO.getName());

    }
}
