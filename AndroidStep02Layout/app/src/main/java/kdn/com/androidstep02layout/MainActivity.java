package kdn.com.androidstep02layout;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import kdn.com.common.util.BackPressCloseHandler;
import kdn.com.common.util.Util;

public class MainActivity extends AppCompatActivity {
    private Button testBtn, bnt02;
    private BackPressCloseHandler closeHandler;

    View.OnClickListener listener = new View.OnClickListener(){
        public void onClick(View v){
            if(v.getId() == R.id.bnt01) {//xml에
                Util.showAlert(MainActivity.this, "bnt01");
            }
            if(v.getId() == R.id.bnt02){
               Util.showToast(MainActivity.this, "second event button");
            }
        }
    };


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        testBtn = (Button)findViewById(R.id.bnt01);
        bnt02 = (Button)findViewById(R.id.bnt02);

        testBtn.setOnClickListener(listener);
        bnt02.setOnClickListener(listener);

        closeHandler = new BackPressCloseHandler(MainActivity.this);

       // testBtn.setOnClickListener(listener);
    }

    int count = 1;
    @Override
    public boolean onKeyDown(int keycode , KeyEvent event){
        if(keycode == KeyEvent.KEYCODE_BACK){
            closeHandler.onBackPressed();
//            AlertDialog.Builder alertDig = new AlertDialog.Builder(this);
//            alertDig.setMessage("한번만 더 누르면 나간다");
////            alertDig.setPositiveButton("예", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int bnt){
//                   System.exit(0);
//                }
//            });
//            alertDig.setNegativeButton("ㄴㄴ", new DialogInterface.OnClickListener(){
//                public void onClick(DialogInterface dialog, int bnt){
//                    dialog.cancel();
//                }
//            });
//
//            alertDig.create().show();
        }
       return false;
    }
}
