package model.VO.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.zip.Inflater;

import kdn.com.kdnappweb.R;
import model.VO.VO.loginVO;

/**
 * Created by User on 2016-05-02.
 */
public class LoginAdapter extends BaseAdapter{

    private Context context;
    private int layoutRes;
    private ArrayList<loginVO> loginVOs;
    private LayoutInflater inflater;

    public LoginAdapter(){

    }

    public LoginAdapter(Context context, int layoutRes, ArrayList<loginVO> loginVOs){
        this.context = context;
        this.layoutRes = layoutRes;
        this.loginVOs = loginVOs;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return loginVOs.size();
    }

    @Override
    public Object getItem(int i) {
        return loginVOs.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        if(view == null){
            view = inflater.inflate(layoutRes, viewGroup, false);
        }

        TextView textView01 = (TextView)view.findViewById(R.id.textView01);
        TextView textView02 = (TextView)view.findViewById(R.id.textView02);
        TextView textView03 = (TextView)view.findViewById(R.id.textView03);

        loginVO dto = loginVOs.get(position);

        textView01.setText(dto.getId());
        textView02.setText(dto.getPwd());
        textView03.setText(dto.getName());

        return view;
    }
}
