package kdn.com.kdnappweb;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import model.VO.Adapter.LoginAdapter;
import model.VO.VO.loginVO;

public class MainActivity extends AppCompatActivity {
    private EditText id;
    private EditText passwd;
    private Button login;
    private LoginAdapter adapter;
    private ArrayList<loginVO> arrayL = new ArrayList<loginVO>();
    private ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView =  (ListView) findViewById(R.id.listView);
        //webView = (WebView) findViewById(R.id.webView);
        id = (EditText) findViewById(R.id.id);
        passwd = (EditText) findViewById(R.id.passwd);
        login = (Button) findViewById(R.id.login);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String loginId = id.getText().toString();
                String loginPasswd = passwd.getText().toString();

                loginProcess(loginId, loginPasswd);
            }
        }); // 3. 1과 2를 연결

        adapter = new LoginAdapter(MainActivity.this, R.layout.listview_cell, arrayL);
        listView.setAdapter(adapter);
        // 안드로이드에서 외부 네트워크를 사용할수 있도록 설정부분
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);
        // 보안설정 꼭 해줘야한다. AndroidManifest.xml 에 uses-permission permission 걸어줘야지
        // 안드로이드에서 사용할 것들을 정의
    }

    private void printLog(){
        for(int i = 0 ; i < arrayL.size(); i++){
            Log.i( "id" , arrayL.get(i).getId() );
            Log.i( "pass" , arrayL.get(i).getPwd());
            Log.i( "name" , arrayL.get(i).getName());
        }
    }

    private Handler handler = new Handler(){
        public void handleMessage(Message msg) {
            readJSON((String)msg.obj);
        }
    };

    private void readJSON(String json) {
        //        TextView text  = (TextView)findViewById(R.id.textView);


//        text.setText("출력결과\n\n");

        try{
            JSONArray jsonArray = new JSONArray(json);
            for(int i=0 ; i < jsonArray.length() ; i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                arrayL.add(new loginVO(jsonObject.getString("id") ,
                                        jsonObject.getString("pwd") ,
                                        jsonObject.getString("name")));
            }
        }catch(Exception e) {
            e.printStackTrace();
        } finally {
            printLog();
        }
    }

    private void loginProcess(String loginId, String loginPasswd) {
        // TODO Auto-generated method stub
        InputStream is = null;
//        String url = "http://192.168.0.10/AndroidWEB/login.jsp";
        String url = "http://10.220.50.163:80/AppWebPJT/android.jsp";

        // HttpClient객체 생성부분
        HttpClient client = new DefaultHttpClient();

        try {
            // NameValuePair 타입의 객체를 저장할수 있는 ArrayList객체를 생성하는 부분
            // ArrayList : 순서대로 저장하는 자료구조형
            ArrayList<NameValuePair> nameValueArr = new ArrayList<NameValuePair>();
            // NameValuePair 하위 클래스 BasicNameValuePair
            nameValueArr.add(new BasicNameValuePair("id", loginId));
            nameValueArr.add(new BasicNameValuePair("pwd", loginPasswd));

            String result = "";

            // post방식으로 로그인 요청 url을 파라미터로 HttpPost객체 생성부분
            HttpPost post = new HttpPost(url);

            // 전송되는 값들의 인코딩 방식을 지정하기 위해서 UrlEncodedFormEntity객체 생성부분
            UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(
                    nameValueArr, "UTF-8");

            // HttpPost 객체에 전송할 HttpEnity를 지정하는 부분
            post.setEntity(urlEncodedFormEntity);

            // 로그인 요청 전송한 후 HttpResponse 타입으로 응답데이터를 리턴받는 부분
            HttpResponse response = client.execute(post);

            // response.getEntity().getContent() 메소드를 사용해서
            // 웹서버에서 응답하는 데이터를 읽을 수 있는
            // 입력 스트림을 리턴 받은후,편리하게 라인단위로
            // 데이터를 읽을 수 있도록 BufferedReader객체를 생성하는 부분
            HttpEntity entityResponse = response.getEntity();
            if(entityResponse != null) {
                String resString = EntityUtils.toString(entityResponse,"UTF-8");
                Message message = handler.obtainMessage();
                message.obj = resString ;
                handler.sendMessage(message);
            }
            /*
            is = entityResponse.getContent();
            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(is, "euc-kr"), 8);
            // 2번째 인수는 1번째 인수의 결과를 8 바이트식 읽도록 함

            // 값을 누적시키는 역할
            StringBuilder sb = new StringBuilder();
            String line = null;

            while ((line = reader.readLine()) != null) {// 한줄씩 값을 불러옴
                sb.append(line).append("\n");
            }
            is.close();
            result = sb.toString(); // 누적된 결과는 String 형으로 형변환
            webView.loadData(result, "text/html", "UTF-8");

            Toast.makeText(getApplicationContext(),"로그인 성공",Toast.LENGTH_SHORT).show();
            */
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            client.getConnectionManager().shutdown();
        }
    }
}
