package practice.kdn.com.picturevote;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {



    int imgTv[] = {
            R.id.imgView1, R.id.imgView2, R.id.imgView3,
            R.id.imgView4, R.id.imgView5, R.id.imgView6,
            R.id.imgView7, R.id.imgView8, R.id.imgView9
    };

    String imgNames[] = {
            "독서하는 소녀", "꽃장식 모자 소녀", "부채를 든 소녀",
            "이레느깡 단 베르양", "잠자는 소녀", "테라스의 두 자매",
            "피아노 레슨", "피아노 앞의 소녀들", "해변에서"
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView imgView[] = new ImageView[9];
        final int cnt[] = new int[9];

        for(int i = 0; i < cnt.length; i++){
            cnt[i] = 0;
        }

        for(int i = 0; i < imgTv.length; i++){
            final int index = i;
            imgView[index] = (ImageView)findViewById(imgTv[index]);
            imgView[index].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        cnt[index]++;
                        Toast.makeText(MainActivity.this, imgNames[index] + " votes : " + cnt[index]
                                , Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
