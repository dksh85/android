package kdn.com.androidstep05adapter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ListViewCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<String> list;
    //<> 1. One datatype input
    //   2. Auto down-casting
    //   homegeneous collection
    private ListView listView;

    public void aryInit(){
        list = new ArrayList<String>();
        list.add("섭섭해");
        list.add("임섭순");
        list.add("이순신");
        list.add("이호길");list.add("강현중");list.add("김수민");
        list.add("김새롬"); list.add("송용호"); list.add("마동석");
        list.add("용"); list.add("말"); list.add("소");

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        aryInit();
        listView = (ListView)findViewById(R.id.list);

        final ArrayAdapter<String> adapter = new
                ArrayAdapter<String>(MainActivity.this,
                    android.R.layout.simple_list_item_1, list);

        listView.setAdapter(adapter);

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener(){
            @Override
            public boolean onItemLongClick(AdapterView<?> parent,
                                           View view, int position, long id) {

                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                intent.putExtra("imageName", R.drawable.tae);
                intent.putExtra("itemName", list.get(position));
                startActivity(intent);

                /*
                Toast.makeText(MainActivity.this, list.get(position) + " welcome",
                        Toast.LENGTH_SHORT).show();
                        */


               /* list.remove(position);
                adapter.notifyDataSetChanged();
*/


                return false;
            }
        });
    }
}
