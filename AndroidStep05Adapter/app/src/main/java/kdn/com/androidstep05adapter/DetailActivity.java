package kdn.com.androidstep05adapter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class DetailActivity extends AppCompatActivity {
    private ImageView imgView;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Intent intent = getIntent();
        int imageDir = intent.getIntExtra("imageName", 0);
        String userName = intent.getStringExtra("itemName");

//        Toast.makeText(DetailActivity.this, userName, Toast.LENGTH_SHORT).show();

        imgView = (ImageView) findViewById(R.id.img);
        imgView.setImageResource(imageDir); // setting dynamically pictures

        textView = (TextView) findViewById(R.id.greeting);
        textView.setText("hi " + userName);


    }
}
