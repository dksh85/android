package com.common.util;

import android.app.AlertDialog;
import android.content.Context;
import android.widget.Toast;

/**
 * Created by User on 2016-04-26.
 */
public class Util {
    public static void showAlert(Context context, String msg){
        //알림창에 띄워보기
        new AlertDialog.Builder(context)
                .setTitle("알림")
                .setMessage(msg)
                .setNeutralButton("확인", null)
                .create()
                .show();
    }
    //토스트 메세지를 띄우는 메소드
    public static void showToast(Context context, String msg){
        //인자로 전달받은 객체를 이용해서 Toast 띄우기
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

}
