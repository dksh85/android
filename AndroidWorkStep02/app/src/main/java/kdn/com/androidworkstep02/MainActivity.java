package kdn.com.androidworkstep02;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.common.util.Util;

public class MainActivity extends AppCompatActivity {
    EditText edit1, edit2;
    Button btn1, btn2, btn3, btn4, btn5;
    TextView textResult;
    String num11, num12;


    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            num11 = edit1.getText().toString();
            num12 = edit2.getText().toString();

            double num1 = Double.parseDouble(num11);
            double num2 = Double.parseDouble(num12);
            double result = 0;

            if(edit1.getText() != null && edit2.getText() != null) {
                if( v.getId() != R.id.btn4) {
                    switch (v.getId()) {
                        case R.id.btn1:
                            result = num1 + num2;
                            break;
                        case R.id.btn2:
                            result = num1 - num2;
                            break;
                        case R.id.btn3:
                            result = num1 * num2;
                            break;
                        case R.id.btn4:
                            result = num1 / num2;
                            break;
                        case R.id.btn5:
                            result = num1 % num2;
                            break;
                    }
                    textResult.setText("계산 결과 :" + Double.toString(result));
                }
                else{
                    if(num2 == 0){
                        Util.showAlert(MainActivity.this, "Divide with 0");
                    }
                    else{
                        result = num1/num2;
                        textResult.setText("계산 결과 :" + Double.toString(result));
                    }

                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("초간단 계산기");

        edit1 = (EditText) findViewById(R.id.Edit1);
        edit2 = (EditText) findViewById(R.id.Edit2);
        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        btn3 = (Button) findViewById(R.id.btn3);
        btn4 = (Button) findViewById(R.id.btn4);
        btn5 = (Button) findViewById(R.id.btn5);
        textResult = (TextView)findViewById(R.id.textResult);


        btn1.setOnClickListener(listener);
        btn2.setOnClickListener(listener);
        btn3.setOnClickListener(listener);
        btn4.setOnClickListener(listener);
        btn5.setOnClickListener(listener);
        textResult.setOnClickListener(listener);
    }

}
