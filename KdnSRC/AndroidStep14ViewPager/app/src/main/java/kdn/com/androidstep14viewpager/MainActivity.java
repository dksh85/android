package kdn.com.androidstep14viewpager;

import android.graphics.drawable.ColorDrawable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
    public static final int MAX_PAGE = 3;
    private Fragment currentFlag = new Fragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ViewPager pager = (ViewPager)findViewById(R.id.viewpager);
        pager.setAdapter(new UserFragAdapter(getSupportFragmentManager()));

    }

    class UserFragAdapter extends FragmentPagerAdapter {

        public UserFragAdapter(FragmentManager fm){
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if(position < 0 || MAX_PAGE <= position) {
                return null;
            }
                switch (position){
                    case 0 :
                        currentFlag = new MyPage01();
                        break;
                    case 1 :
                        currentFlag = new MyPage02();
                        break;
                    case 2 :
                        currentFlag = new MyPage03();
                        break;
                }

            return currentFlag;
        }

        @Override
        public int getCount() {
            return MAX_PAGE;
        }
    }

    public static class MyPage01 extends Fragment {
        public void onCreate(@Nullable  Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            LinearLayout linearLayout =
                    (LinearLayout)inflater.inflate(R.layout.activity_sliding,container, false);
            LinearLayout background = (LinearLayout)linearLayout.findViewById(R.id.background);
            TextView textView = (TextView)linearLayout.findViewById(R.id.pageNum);
            textView.setText("Fragment01");
//            background.setBackground(new ColorDrawable(#6799FF));
            return linearLayout;
        }
    }

        public static class MyPage02 extends Fragment {
        public void onCreate(@Nullable  Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            LinearLayout linearLayout =
                    (LinearLayout)inflater.inflate(R.layout.activity_sliding,container, false);
            LinearLayout background = (LinearLayout)linearLayout.findViewById(R.id.background);
            TextView textView = (TextView)linearLayout.findViewById(R.id.pageNum);
            textView.setText("Fragment02");
//            background.setBackground(new ColorDrawable(#6799FF));
            return linearLayout;
        }
    }

    public static class MyPage03 extends Fragment {
        public void onCreate(@Nullable  Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            LinearLayout linearLayout =
                    (LinearLayout)inflater.inflate(R.layout.activity_sliding,container, false);
            LinearLayout background = (LinearLayout)linearLayout.findViewById(R.id.background);
            TextView textView = (TextView)linearLayout.findViewById(R.id.pageNum);
            textView.setText("Fragment03");
//            background.setBackground(new ColorDrawable(#6799FF));
            return linearLayout;
        }
    }
}