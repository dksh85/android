package kdn.com.kdnnfc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class SecondActivity extends AppCompatActivity {

    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Intent intent = getIntent();

        ArrayList<String> list = (ArrayList<String>)intent.getSerializableExtra("ary");
        listView = (ListView)findViewById(R.id.listView);

        ArrayAdapter<String> adapter
                = new ArrayAdapter<String> (this, android.R.layout.simple_list_item_1, list);

        listView.setAdapter(adapter);
    }
}
