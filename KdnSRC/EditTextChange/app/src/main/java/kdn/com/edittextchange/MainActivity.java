package kdn.com.edittextchange;

import android.app.AlertDialog;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

import model.ArithmeticAdapter.ArithmeticAdaptor;
import model.ArithmeticVO;

public class MainActivity extends AppCompatActivity {
    Button btn;
    EditText editText1, editText2, result_text;
    ArrayList<ArithmeticVO> aList;

    ListView listView;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        aList = new ArrayList<ArithmeticVO>();

        aList.add(new ArithmeticVO("sum", R.drawable.sum));
        aList.add(new ArithmeticVO("minus", R.drawable.minus));
        aList.add(new ArithmeticVO("multiplication", R.drawable.sum));
        aList.add(new ArithmeticVO("division", R.drawable.sum));

        editText1 = (EditText)findViewById(R.id.enditText1);
        editText2 = (EditText)findViewById(R.id.enditText2);
        result_text = (EditText)findViewById(R.id.result_op);
        btn = (Button)findViewById(R.id.btn);

        final ArithmeticAdaptor listAdaptor = new ArithmeticAdaptor(MainActivity.this, R.layout.listview_item, aList);


//        View dialogView = (View)View.inflate(MainActivity.this, R.layout.listview_layout, null);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View forList = View.inflate(MainActivity.this, R.layout.listview_layout, null);
                listView = (ListView) forList.findViewById(R.id.listView1);
                listView.setAdapter(listAdaptor);
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("hi");
                builder.setView(forList);
                builder.setNeutralButton("ok", null);
                builder.show();

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String operation = aList.get(position).getOperation();
                        btn.setText(operation);



                        String a = editText1.getText().toString();
                        String b = editText2.getText().toString();

                        int a1 = Integer.parseInt(a);
                        int b1 = Integer.parseInt(b);
                        int sum = -1;

                        if (operation.equals("sum")){
                            sum = a1 + b1;
                        }
                        else if(operation.equals("minus")){
                            sum = a1 - b1;
                            Log.i("in minus", "in minus");
                        }
                        result_text.setText(Integer.toString(sum));
                    }
                });


            }
        });




    }
}
