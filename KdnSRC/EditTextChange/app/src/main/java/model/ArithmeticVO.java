package model;

/**
 * Created by User on 2016-05-12.
 */
public class ArithmeticVO {
    private String operation;
    private int imgRes;

    public ArithmeticVO(String operation, int imgRes){
        this.operation = operation;
        this.imgRes = imgRes;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public int getImgRes() {
        return imgRes;
    }

    public void setImgRes(int imgRes) {
        this.imgRes = imgRes;
    }
}
