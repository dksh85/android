package model.ArithmeticAdapter;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.zip.Inflater;

import kdn.com.edittextchange.R;
import model.ArithmeticVO;

/**
 * Created by User on 2016-05-12.
 */
public class ArithmeticAdaptor extends BaseAdapter {
    private Context context;
    private ArrayList<ArithmeticVO> aList;
    private int layoutRes;
    private LayoutInflater inflater;

    public ArithmeticAdaptor (Context context, int layoutRes, ArrayList<ArithmeticVO> aList){
        this.context = context;
        this.layoutRes = layoutRes;
        this.aList = aList;

        this.inflater = LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return aList.size();
    }

    @Override
    public Object getItem(int position) {
        return aList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = inflater.inflate(layoutRes, parent, false);
        }

        ImageView imgView = (ImageView) convertView.findViewById(R.id.imgView);
        TextView  txtView = (TextView) convertView.findViewById(R.id.txtView);

        ArithmeticVO a = aList.get(position);

        imgView.setImageResource(a.getImgRes());
        txtView.setText(a.getOperation());

        return convertView;
    }
}
