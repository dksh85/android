package kdn.com.androidstep13async;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button btn;
    private ProgressBar bar;
    private AsyncTask<Void, Integer, Void> mTask;

    View.OnClickListener btnHandler = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            if(btn.getText().equals("START")){
                btn.setText("CANCEL");
                    mTask = new AsyncTask<Void, Integer, Void>() {
                        private boolean isCancelled = false;


                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                            System.out.println("***************************** onPreExecute");
                            isCancelled = false;
                        }

                        @Override
                        protected Void doInBackground(Void... params) {
                            System.out.println("***************************** doInBackground");
                            for(int i = 1; i <= 100 && !isCancelled ; i++){
                                try{
                                    publishProgress(i);
                                    Thread.sleep(100);
                                } catch(Exception e){
                                    e.printStackTrace();
                                }
                            }
                            return null;
                        }

                        @Override
                        protected void onProgressUpdate(Integer... process) {
                            super.onProgressUpdate(process);  //ui of main
                            bar.setProgress(process[0]);
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) { //On all job done
                            super.onPostExecute(aVoid);
                            Toast.makeText(MainActivity.this, "Jobs done", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        protected void onCancelled() {
                            super.onCancelled();
                            isCancelled = true;
                            Toast.makeText(MainActivity.this, "Jobs cancelled", Toast.LENGTH_SHORT).show();
                        }
                    }; //여기까지가 thread를 정의하는 작업
                    mTask.execute();
                    btn.setText("CANCEL");
                }
                else if (btn.getText().equals("CANCEL")){
                    mTask.cancel(false);
                    btn.setText("START");
                }
            }
        };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn = (Button)findViewById(R.id.btn);
        bar = (ProgressBar)findViewById(R.id.progress);

        btn.setOnClickListener(btnHandler);

    }
}
