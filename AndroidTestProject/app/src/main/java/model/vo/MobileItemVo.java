package model.vo;

/**
 * Created by User on 2016-05-04.
 */
public class MobileItemVo {
    public String mobileName;
    public String year;
    public int resourceId;

    public MobileItemVo(String mobileName, String year, int resourceId) {
        this.mobileName = mobileName;
        this.year = year;
        this.resourceId = resourceId;
    }

    public String getMobileName() {
        return mobileName;
    }

    public void setMobileName(String mobileName) {
        this.mobileName = mobileName;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }
}
