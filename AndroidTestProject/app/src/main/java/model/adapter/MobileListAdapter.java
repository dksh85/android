package model.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import kdn.com.androidtestproject.R;
import model.vo.MobileItemVo;

/**
 * Created by User on 2016-05-04.
 */
public class MobileListAdapter extends BaseAdapter {
    private Context context;
    private int layoutRes;
    private ArrayList<MobileItemVo> list;
    private LayoutInflater inflater;

    public MobileListAdapter(Context context, int layoutRes, ArrayList<MobileItemVo> list){
        this.context = context;
        this.layoutRes = layoutRes;
        this.list = list;

        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView=inflater.inflate(layoutRes,parent,false);
        }
        ImageView imageView = (ImageView)convertView.findViewById(R.id.listImg);
        TextView textName = (TextView)convertView.findViewById(R.id.listTxtName);
        TextView textYear = (TextView)convertView.findViewById(R.id.listTxtYear);

        MobileItemVo vo = list.get(position);

        imageView.setImageResource(vo.resourceId);
        textName.setText(vo.getMobileName());
        textYear.setText(vo.getYear());

        return convertView;
    }
}
