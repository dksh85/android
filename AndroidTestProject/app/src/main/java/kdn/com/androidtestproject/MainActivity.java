package kdn.com.androidtestproject;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import model.adapter.MobileListAdapter;
import model.vo.MobileItemVo;

public class MainActivity extends AppCompatActivity {
    private ArrayList<MobileItemVo> list;
    private ListView mMobileListView;
    private AlertDialog mAdlgPopup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mMobileListView = (ListView)findViewById(R.id.listView);

        list = new ArrayList<MobileItemVo>();
        list.add(new MobileItemVo("갤럭시S3","2012",R.drawable.galaxy_s3));
        list.add(new MobileItemVo("갤럭시노트","2011",R.drawable.galaxy_note));
        list.add(new MobileItemVo("갤럭시 넥서스","2011",R.drawable.galaxy_nexus));
        list.add(new MobileItemVo("갤럭시S2","2011",R.drawable.galaxy_s2));
        list.add(new MobileItemVo("넥서스S","2010",R.drawable.nexus_s));

        MobileListAdapter adapter = new MobileListAdapter(MainActivity.this, R.layout.customadapter, list);

        mMobileListView.setAdapter(adapter);
        mMobileListView.setOnItemClickListener(mOnItemClickListener);
    }

    AdapterView.OnItemClickListener mOnItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            MobileItemVo vo = list.get(position);

            AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
            alert.setTitle(vo.getMobileName());
            alert.setMessage(vo.getYear());
            alert.setPositiveButton("OK",null);
            alert.create();
            alert.show();
        }
    };
}
