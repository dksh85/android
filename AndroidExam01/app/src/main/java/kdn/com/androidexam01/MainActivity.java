package kdn.com.androidexam01;

import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    private enum KAWIBAWIBO{
        KAWI, BAWI, BO
    }

    private TextView mTvResult=null;
    private ImageView mIvPlayerAction=null;
    private ImageView mIvComputerAction=null;
    private int mPlyaerScore=0;
    private int mComputerScore=0;
    private KAWIBAWIBO player, computer;
    private Button btnKawi, btnBawi, btnBo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTvResult=(TextView)findViewById(R.id.tvResult);
        mIvPlayerAction=(ImageView)findViewById(R.id.player);
        mIvComputerAction=(ImageView)findViewById(R.id.com);
        btnKawi=(Button)findViewById(R.id.btnKawi);
        btnBawi=(Button)findViewById(R.id.btnBawi);
        btnBo=(Button)findViewById(R.id.btnBo);

        btnKawi.setOnClickListener(mOnClickListener);
        btnBawi.setOnClickListener(mOnClickListener);
        btnBo.setOnClickListener(mOnClickListener);


    }

    private View.OnClickListener mOnClickListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v.getId()==R.id.btnKawi){
                mIvPlayerAction.setImageResource(R.drawable.kawi);
                player=KAWIBAWIBO.KAWI;
            }else if(v.getId()==R.id.btnBawi){
                mIvPlayerAction.setImageResource(R.drawable.bawi);
                player=KAWIBAWIBO.BAWI;
            }else if(v.getId()==R.id.btnBo){
                mIvPlayerAction.setImageResource(R.drawable.bo);
                player=KAWIBAWIBO.BO;
            }

            computer=getComputerAction();
            checkTheGame(player, computer );
            showResult();
        }
    };

    private KAWIBAWIBO getComputerAction(){

        KAWIBAWIBO result=KAWIBAWIBO.KAWI;

        int n=(int)(Math.random()*3);

        switch (n){
            case 0:
                result=KAWIBAWIBO.KAWI;
                mIvComputerAction.setImageResource(R.drawable.kawi);
                break;
            case 1:
                result=KAWIBAWIBO.BAWI;
                mIvComputerAction.setImageResource(R.drawable.bawi);
                break;
            case 2:
                result=KAWIBAWIBO.BO;
                mIvComputerAction.setImageResource(R.drawable.bo);
                break;
        }

        return result;
    }

    private void checkTheGame(KAWIBAWIBO playerAction, KAWIBAWIBO computerAction){

        if(playerAction==KAWIBAWIBO.KAWI){
            if(computerAction==KAWIBAWIBO.BAWI){mComputerScore++;}
            else if(computerAction==KAWIBAWIBO.BO){mPlyaerScore++;}
        }else if(playerAction==KAWIBAWIBO.BAWI){
            if(computerAction==KAWIBAWIBO.BO){mComputerScore++;}
            else if(computerAction==KAWIBAWIBO.KAWI){mPlyaerScore++;}
        }else if(playerAction==KAWIBAWIBO.BO){
            if(computerAction==KAWIBAWIBO.KAWI){mComputerScore++;}
            else if(computerAction==KAWIBAWIBO.BAWI){mPlyaerScore++;}
        }
    }

    private void showResult(){
        mTvResult.setText(mPlyaerScore +" : "+ mComputerScore);
    }
}
