package model.VO.listCustomAdapter;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.zip.Inflater;

import kdn.com.customviewtest.R;
import model.VO.vo.CountryVO;

/**
 * Created by User on 2016-05-03.
 */
public class CountryAdapter extends BaseAdapter{
    private Context context;
    private int layoutRes;
    private LayoutInflater layoutInflater;
    private ArrayList<CountryVO> countryList;

    public CountryAdapter(Context context, int layoutRes, ArrayList<CountryVO> countryList){
        this.context = context;
        this.layoutRes = layoutRes;
        this.countryList = countryList;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return countryList.size();
    }

    @Override
    public Object getItem(int position) {
        return countryList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = layoutInflater.inflate(layoutRes, parent, false);
        }

        ImageView imageView = (ImageView) convertView.findViewById(R.id.imageView);
        TextView textView = (TextView) convertView.findViewById(R.id.textView);

        imageView.setImageResource(countryList.get(position).getImgRes());
        textView.setText(countryList.get(position).getName());


        return convertView;

    }
}
