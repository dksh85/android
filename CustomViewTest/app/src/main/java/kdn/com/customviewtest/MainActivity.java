package kdn.com.customviewtest;

import android.app.AlertDialog;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import model.VO.listCustomAdapter.CountryAdapter;
import model.VO.vo.CountryVO;

public class MainActivity extends AppCompatActivity {

    ArrayList<CountryVO> arrayList;
    ListView listView;
    CountryAdapter countryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        arrayList = new ArrayList<CountryVO>();
        arrayList.add(new CountryVO(R.drawable.austria, "오스트리아"));
        arrayList.add(new CountryVO(R.drawable.belgium, "벨기에"));
        arrayList.add(new CountryVO(R.drawable.brazil, "브라질"));
        arrayList.add(new CountryVO(R.drawable.france, "프랑스"));
        arrayList.add(new CountryVO(R.drawable.germany, "독일"));
        arrayList.add(new CountryVO(R.drawable.greece, "그리스"));
        arrayList.add(new CountryVO(R.drawable.israel, "이스라엘"));
        arrayList.add(new CountryVO(R.drawable.italy, "이탈리아"));
        arrayList.add(new CountryVO(R.drawable.japan, "일본"));
        arrayList.add(new CountryVO(R.drawable.korea, "대한민국"));
        arrayList.add(new CountryVO(R.drawable.poland, "폴란드"));
        arrayList.add(new CountryVO(R.drawable.spain, "스페인"));
        arrayList.add(new CountryVO(R.drawable.usa, "미국"));

        listView = (ListView)findViewById(R.id.listView);

        countryAdapter = new CountryAdapter(MainActivity.this, R.layout.list_item, arrayList);

        listView.setAdapter(countryAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               AlertDialog.Builder diag = new AlertDialog.Builder(MainActivity.this);
                View diagView = (View)View.inflate(MainActivity.this, R.layout.dialog, null);
                ImageView imageView = (ImageView) diagView.findViewById(R.id.ivPoster);
                CountryVO countryVO = arrayList.get(position);
                imageView.setImageResource(countryVO.getImgRes());
                diag.setNeutralButton("ok", null);
                diag.setTitle("Detail");
                diag.setView(diagView);
                diag.show();
            }
        });
    }
}
