package model.vo;

import java.io.Serializable;

/**
 * Created by User on 2016-05-02.
 */
public class CountryVO implements Serializable{
    private int imgRes;
    private String  name;

    public CountryVO(){
    }

    public CountryVO(int imgRes, String name){
        this.imgRes = imgRes;
        this.name = name;
    }

    public int getImgRes() {
        return imgRes;
    }

    public void setImgRes(int imgRes) {
        this.imgRes = imgRes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
