package model.vo.CountryAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import kdn.com.myapplication.R;
import model.vo.CountryVO;

/**
 * Created by User on 2016-05-04.
 */
public class CountryAdapter extends BaseAdapter {
    private ArrayList<CountryVO> countryList;
    private int layoutRes;
    private Context context;
    private LayoutInflater layoutInflater;

    public CountryAdapter( Context context , int layoutRes, ArrayList<CountryVO> countryList) {
        this.context = context;
        this.layoutRes = layoutRes;
        this.countryList = countryList;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return countryList.size();
    }

    @Override
    public Object getItem(int position) {
        return countryList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = layoutInflater.inflate(layoutRes, parent, false);
        }

        CountryVO countryVO = countryList.get(position);

        ImageView imageView = (ImageView) convertView.findViewById(R.id.imageView);
        TextView textView = (TextView) convertView.findViewById(R.id.textView);
        Button btn = (Button) convertView.findViewById(R.id.btn);

        imageView.setImageResource(countryVO.getImgRes());
        textView.setText(countryVO.getName());

        return convertView;
    }
}
