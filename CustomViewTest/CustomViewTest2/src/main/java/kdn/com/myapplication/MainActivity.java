package kdn.com.myapplication;

import android.app.AlertDialog;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;

import model.vo.CountryAdapter.CountryAdapter;
import model.vo.CountryVO;

public class MainActivity extends AppCompatActivity {
    ArrayList<CountryVO> countryList;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.listView);

        countryList = new ArrayList<CountryVO>();

        countryList.add(new CountryVO(R.drawable.austria, "오스트리아"));
        countryList.add(new CountryVO(R.drawable.belgium, "벨기에"));
        countryList.add(new CountryVO(R.drawable.brazil, "브라질"));
        countryList.add(new CountryVO(R.drawable.france, "프랑스"));
        countryList.add(new CountryVO(R.drawable.germany, "독일"));
        countryList.add(new CountryVO(R.drawable.greece, "그리스"));
        countryList.add(new CountryVO(R.drawable.israel, "이스라엘"));
        countryList.add(new CountryVO(R.drawable.italy, "이탈리아"));
        countryList.add(new CountryVO(R.drawable.japan, "일본"));
        countryList.add(new CountryVO(R.drawable.korea, "대한민국"));
        countryList.add(new CountryVO(R.drawable.poland, "폴란드"));
        countryList.add(new CountryVO(R.drawable.spain, "스페인"));
        countryList.add(new CountryVO(R.drawable.usa, "미국"));

        CountryAdapter countryAdapter = new CountryAdapter(MainActivity.this, R.layout.view_item, countryList);

        listView.setAdapter(countryAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                View dialogView = (View) View.inflate(MainActivity.this, R.layout.view_dialog, null);
                CountryVO countryVO = countryList.get(position);
                ImageView imageView = (ImageView) dialogView.findViewById(R.id.imageDiag);
                imageView.setImageResource(countryVO.getImgRes());

                AlertDialog.Builder diag = new AlertDialog.Builder(MainActivity.this);
                diag.setTitle("Detail");
                diag.setNeutralButton("ok", null);
                diag.setView(dialogView);
                diag.show();
            }
        });
    }
}
