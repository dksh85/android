package kdn.com.androidstep09service;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    public static final String MYSERVICE = "kdn.com.androidstep09service.MYSERVICE";
    public static final String MYNEWSERVICE = "kdn.com.androidstep09service.MYNEWSERVICE";

    private Button startBtn, endBtn, startNewBtn, endNewBtn;


    View.OnClickListener btnHandler = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            if(v.getId() == R.id.myServiceBtn){
                Intent intent = new Intent();
                intent.setAction(MYSERVICE);
                startService(intent); //onStartService() callback
            }else{
                Intent intent = new Intent();
                intent.setAction(MYSERVICE);
                stopService(intent); //onStartService() callback
            }
        }
    };

    View.OnClickListener newbtnHandler = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            if(v.getId() == R.id.myServiceNewStartBtn){
                Intent intent = new Intent();
                intent.setAction(MYNEWSERVICE);
                startService(intent); //onStartService() callback
            }else{
                Intent intent = new Intent();
                intent.setAction(MYNEWSERVICE);
                stopService(intent); //onStartService() callback
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startBtn = (Button)findViewById(R.id.myServiceBtn);
        endBtn = (Button)findViewById(R.id.myServiceEndBtn);
        startBtn.setOnClickListener(btnHandler);
        endBtn.setOnClickListener(btnHandler);
        startNewBtn = (Button)findViewById(R.id.myServiceNewStartBtn);
        endNewBtn = (Button)findViewById(R.id.myServiceNewEndBtn);
        startNewBtn.setOnClickListener(newbtnHandler);
        endNewBtn.setOnClickListener(newbtnHandler);


    }
}
