package kdn.com.androidstep09service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.widget.Toast;

public class ActivityMyService extends Service {
    public ActivityMyService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    int cnt = 0;
    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(msg.what==0){
                cnt++;
                Toast.makeText(getApplicationContext() ,
                        cnt+"번 호출됨", Toast.LENGTH_SHORT).show();
                handler.sendEmptyMessageDelayed(0, 5000);
            }
        }
    };

//Backgroud 로 돌기 시작 handler 구현해야 함
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        handler.sendEmptyMessage(0); //what
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        handler.removeMessages(0);
        super.onDestroy();
    }
}
