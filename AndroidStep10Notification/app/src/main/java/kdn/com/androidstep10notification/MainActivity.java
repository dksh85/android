package kdn.com.androidstep10notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private Button btn;
    public static final int NOTI_ID01 = 9999;

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            showNotification();
        }
    };

    public void showNotification(){
//        Intent intent = new Intent(MainActivity.this, MainActivity.class);
        Intent intent = new Intent(MainActivity.this, NotiResultActivity.class);
        //알림메뉴를 누르면 해당 activity로 간다.
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // 깨우는 작업

        PendingIntent pendingIntent = PendingIntent.getActivity(
                MainActivity.this,
                0,
                intent ,
                0
        );

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(MainActivity.this);

        //notification 만들기
        builder.setContentIntent(pendingIntent);
        builder.setSmallIcon(R.drawable.ic_launcher);
        builder.setContentTitle("알림");
        builder.setContentText("알림내용입니다");
        builder.setTicker("상단의 간단 메세지");
        builder.setAutoCancel(true);

        Notification noti = builder.build();
        //builder 속성
        noti.defaults = (Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);

        NotificationManager manager =
                (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(NOTI_ID01, noti); //상단알림 뜸
//        manager.cancel(); //destroy() call
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.testNoti).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.sendEmptyMessageDelayed( 0 , 10000);
            }
        });
    }
}
