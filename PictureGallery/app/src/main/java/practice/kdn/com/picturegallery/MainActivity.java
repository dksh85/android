package practice.kdn.com.picturegallery;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    ListView picView;
    ImageView imgView;
    int pickPicNum;

    public enum picEnum{
        PIC1, PIC2, PIC3,
        PIC4, PIC5, PIC6,
        PIC7, PIC8, PIC9
    }
    public int randomPic(){
        int nan = (int)(Math.random()*9)+1;
        return nan;
/*
        int picNum=0;
        picEnum pResult = null;

        Log.i("show show", Integer.toString(nan));

        switch(nan){
            case 1 : picNum = R.drawable.pic1; break;
            case 2 : picNum = R.drawable.pic2; break;
            case 3 : picNum = R.drawable.pic3; break;
            case 4 : picNum = R.drawable.pic4; break;
            case 5 : picNum = R.drawable.pic5; break;
            case 6 : picNum = R.drawable.pic6; break;
            case 7 : picNum = R.drawable.pic7; break;
            case 8 : picNum = R.drawable.pic8; break;
            case 9 : picNum = R.drawable.pic9; break;
        }
        return picNum;
*/

       /*     if(pict == pResult && pict == picEnum.PIC1) {
                return R.drawable.pic1;
            }
            else if(pict == pResult && pResult == picEnum.PIC2) {
                return R.drawable.pic2;
            }
            else if(pict == pResult && pResult == picEnum.PIC3) {
                return R.drawable.pic3;
            }
        return 0;*/
    }


    final String imgName[] = {
            "독서하는 소녀", "꽃장식 모자 소녀", "부채를 든 소녀",
            "이레느깡 단 베르양", "잠자는 소녀", "테라스의 두 자매",
            "피아노 레슨", "피아노 앞의 소녀들", "해변에서"
    };

    final int imgId[] = {
            R.drawable.pic1, R.drawable.pic2, R.drawable.pic3,
            R.drawable.pic4, R.drawable.pic5, R.drawable.pic6,
            R.drawable.pic7, R.drawable.pic8, R.drawable.pic9
    };



    ListView.OnItemClickListener listViewhandler = new ListView.OnItemClickListener(){
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Log.i("number = ", Integer.toString(pickPicNum));

            if(pickPicNum == position+1){
                Toast.makeText(MainActivity.this, "정답입니다! ", Toast.LENGTH_SHORT).show();
                pickPicNum = randomPic();
                imgView.setImageResource(imgId[pickPicNum-1]);
            }
            else{
                Toast.makeText(MainActivity.this, "오답입니다! ", Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        picView = (ListView)findViewById(R.id.picView);
        imgView = (ImageView)findViewById(R.id.imageView);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this,
                android.R.layout.simple_list_item_1, imgName );

        picView.setAdapter(adapter);

        pickPicNum = randomPic();
        imgView.setImageResource(imgId[pickPicNum-1]);

        picView.setOnItemClickListener(listViewhandler);
    }
}
