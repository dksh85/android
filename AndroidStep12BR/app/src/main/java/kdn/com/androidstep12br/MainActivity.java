package kdn.com.androidstep12br;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btn).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction("kdn.com.androidstep12br.sendBR");
                //추가
                sendBroadcast(intent);
            }
        });
    }

    public void showToast(String msg){
       Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
    }
}
