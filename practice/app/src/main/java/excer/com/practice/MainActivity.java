package excer.com.practice;

import android.content.Intent;
import android.graphics.Picture;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    Button btnResult;
    final int voteCnt[] = new int[9];

    View.OnClickListener resBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            List<PictureC> picList = new ArrayList<PictureC>();
            if(v.getId() == R.id.btnResult){
                for(int i = 0; i < imgName.length; i++){
                    picList.add(new PictureC(imgName[i], voteCnt[i]));
                }
                Intent intent = new Intent(MainActivity.this, SubActivity.class);
                intent.putExtra("list", (Serializable) picList);
                startActivity(intent);
            }
        }
    };

    String imgName[] = {
                            "독서하는 소녀", "꽃장식 모자 소녀", "부채를 든 소녀",
                            "이레느깡 단 베르양", "잠자는 소녀", "테라스의 두 자매",
                            "피아노 레슨", "피아노 앞의 소녀들", "해변에서"
                        };

    int imgId[] = {
                        R.id.iv1, R.id.iv2, R.id.iv3,
                        R.id.iv4, R.id.iv5, R.id.iv6,
                        R.id.iv7, R.id.iv8, R.id.iv9
                    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView imgView[] = new ImageView[9];


        btnResult = (Button)findViewById(R.id.btnResult);
        btnResult.setOnClickListener(resBtnListener);

        for(int i = 0; i<voteCnt.length; i++){
            voteCnt[i]= 0;
        }

        for(int i = 0; i < imgName.length; i++){
            final int index = i;
            imgView[i] = (ImageView)findViewById(imgId[i]);
            imgView[i].setOnClickListener(new View.OnClickListener(){

                @Override
                public void onClick(View v) {
                    voteCnt[index]++;
                    Toast.makeText(MainActivity.this, imgName[index] + " : "+ voteCnt[index] , Toast.LENGTH_LONG).show();
                }
            });
        }
    }
}
