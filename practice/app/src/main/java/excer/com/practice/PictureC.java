package excer.com.practice;

import java.io.Serializable;

/**
 * Created by User on 2016-04-27.
 */
public class PictureC implements Serializable {
    private String imgName;
    private int imgRating;

    public PictureC(String imgName) {
        this.imgName = imgName;
    }

    public PictureC(String imgName, int imgRating) {
        this.imgName = imgName;
        this.imgRating = imgRating;
    }

    public String getImgName() {
        return imgName;
    }

    public void setImgName(String imgName) {
        this.imgName = imgName;
    }

    public int getImgRating() {
        return imgRating;
    }

    public void setImgRating(int imgRating) {
        this.imgRating = imgRating;
    }
}
