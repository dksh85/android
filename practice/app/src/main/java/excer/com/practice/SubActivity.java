package excer.com.practice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

public class SubActivity extends AppCompatActivity {

    int tv[] = {
                    R.id.tv1, R.id.tv2, R.id.tv3,
                    R.id.tv4, R.id.tv5, R.id.tv6,
                    R.id.tv7, R.id.tv8, R.id.tv9
               };

    int rtv[] = {
                    R.id.rbar1, R.id.rbar2, R.id.rbar3,
                    R.id.rbar4, R.id.rbar5, R.id.rbar6,
                    R.id.rbar7, R.id.rbar8, R.id.rbar9,
                };

    TextView tvView[] = new TextView[9];
    RatingBar rView[]= new RatingBar[9];

    Button btnHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub);

        Intent intent = getIntent();
        ArrayList<PictureC> picList =
                (ArrayList<PictureC>) intent.getSerializableExtra("list");

        for(int i = 0; i < tv.length; i++){
            tvView[i] =(TextView) findViewById(tv[i]);
            tvView[i].setText(picList.get(i).getImgName());
            rView[i] = (RatingBar) findViewById(rtv[i]);
            rView[i].setRating((float)picList.get(i).getImgRating());
        }

        btnHandler = (Button) findViewById(R.id.btnReturn);
        btnHandler.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
