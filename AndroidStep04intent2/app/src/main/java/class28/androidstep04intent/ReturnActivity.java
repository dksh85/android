package class28.androidstep04intent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ReturnActivity extends AppCompatActivity {
    private Button btn01, btn02;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_return);

        final Intent intent = getIntent();

        btn01 = (Button)findViewById(R.id.returnBtn01);
        btn02 = (Button)findViewById(R.id.returnBtn02);
        btn01.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                String msg = intent.getStringExtra("firstMsg");
                Intent outIntent = new Intent(ReturnActivity.this, IntroActivity.class);
                outIntent.putExtra("returnMsg", msg);
                setResult(RESULT_OK , outIntent);
                finish();
            }
        });
        btn02.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                String msg = intent.getStringExtra("secondMsg");
                Intent outIntent = new Intent(ReturnActivity.this, IntroActivity.class);
                outIntent.putExtra("returnMsg", msg);
                setResult(RESULT_CANCELED , outIntent);
                finish();
            }
        });
    }
}
