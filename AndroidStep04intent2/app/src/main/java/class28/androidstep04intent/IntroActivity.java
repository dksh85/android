package class28.androidstep04intent;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class IntroActivity extends AppCompatActivity {
    public static final int REQUEST_OK = 100;
    public static final int REQUEST_FIRST  = 100;
    public static final int REQUEST_SECOND = 200;
    Button btn01;
    Button btn02;


    View.OnClickListener handler = new View.OnClickListener(){

        public void onClick(View v){
            if(v == btn01){
                Intent intent = new Intent(IntroActivity.this, ReturnActivity.class);
                intent.putExtra("firstMsg", "Passing data...");
                startActivityForResult(intent, REQUEST_FIRST);
            }
            if(v.getId() == R.id.sendBtn02){
                Intent intent = new Intent(IntroActivity.this, ReturnActivity.class);
                intent.putExtra("secondMsg", "Passing data...2");
                startActivityForResult(intent, REQUEST_SECOND);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        btn01 = (Button)findViewById(R.id.sendBtn01);
        btn02 = (Button)findViewById(R.id.sendBtn02);
        btn01.setOnClickListener(handler);
        btn02.setOnClickListener(handler);
    }

    private void resultFirst(int resultCode, Intent data){
        if(resultCode == RESULT_OK){
            new AlertDialog.Builder(IntroActivity.this)
                    .setTitle("alert")
                    .setMessage(data.getStringExtra("returnMsg"))
                    .setNeutralButton("ok", null)
                    .create()
                    .show();
        }
    }

    private void resultSecond(int resultCode, Intent data){
        if(resultCode == RESULT_CANCELED){
            Toast.makeText(IntroActivity.this, data.getStringExtra("returnMsg"), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        //callback method
        switch(requestCode){
            case REQUEST_FIRST :
                resultFirst(resultCode, data);
                break;

            case REQUEST_SECOND :
                resultSecond(resultCode, data);
                break;
        }
    }
}
